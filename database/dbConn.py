import sqlite3
import traceback

#Models a SQLite Database Connection
class DatabaseConnection:
    #initialize class with a connection string (SQLite file)
    def __init__(self, connectionString):
        self.connectionString = connectionString

    #execute one or multiple SQL queries and commit the result
    def execute(self, *args):
        isSuccessful = False

        try:
            conn = sqlite3.connect(self.connectionString)
            if conn is not None:
                c = conn.cursor()
                for ar in args:
                    c.execute(ar)
                conn.commit()
                isSuccessful = True

        except:
            print(traceback.print_exc())

        finally:
            if conn:
                conn.close()
            return isSuccessful

    #executes a query with sanitized input
    def executeSafe(self, query, *args):
        isSuccessful = False

        try:
            conn = sqlite3.connect(self.connectionString)
            if conn is not None:
                c = conn.cursor()
                c.execute(query, args)
                conn.commit()
                isSuccessful = True

        except:
            print(traceback.print_exc())

        finally:
            if conn:
                conn.close()
            return isSuccessful

    #executes a query with sanitized input and returns the id of the data input
    def executeWithReturn(self, query, *args):
        returnId = None

        try:
            conn = sqlite3.connect(self.connectionString)
            if conn is not None:
                c = conn.cursor()
                c.execute(query, args)
                conn.commit()
                returnId = c.lastrowid

        except:
            print(traceback.print_exc())

        finally:
            if conn:
                conn.close()
            return returnId

    #query an SQL string and return the result
    def query(self, query):
        conn = sqlite3.connect(self.connectionString)
        try:
            if conn is not None:
                c = conn.cursor()
                c.execute(query)
                l = c.fetchall()
                conn.close()
                return l

        except:
            print(traceback.print_exc())

        finally:
            if conn:
                conn.close()