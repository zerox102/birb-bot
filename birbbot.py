import warnings
import sqlite3
import keyring
import discord
import asyncio
from database.dbConn import DatabaseConnection
from datetime import date
#use of loop argument in client.wait_for...
warnings.filterwarnings("ignore", category=DeprecationWarning) 


#constants
BOT_TOKEN = keyring.get_password("birbbot", "username")
DB_NAME = "birbdb.db"

#global variables
admins = []
authorized_users = []
set_flags = dict()
fights = dict()
players = dict()
farms = dict()
attendance = dict()

#Initialize dbConnection class
dbConnection = DatabaseConnection(DB_NAME)

class MyClient(discord.Client):
    async def on_ready(self):
        global authorized_users
        global set_flags
        global admins
        global fights
        global players
        global farms
        global attendance

        print('Logged on as', self.user)
        check_db_exists()
        print('DB loaded successfully.')
        authorized_users = load_authorized_users()
        print('Command Authorized Users: ', authorized_users)
        admins = load_admins()
        print('Admins: ', admins)
        set_flags = load_bot_settings()
        print('Loaded flags from DB: ', set_flags)
        fights = load_fights()
        print('Loaded fights from DB: ', fights)
        players = load_players()
        print('Loaded users from DB: ', players)
        farms = load_farms()
        print('Loaded farms from DB: ', farms)
        attendance = load_attendance()
        print('Loaded attendances from DB: ', attendance)


    async def on_message(self, message):
        if message.author == self.user:
            return
        
        if not authorized_users and message.content.startswith('birb~') and message.content != "birb~authorize":
            await message.channel.send("To use the bot, please first setup an intial authorized user by typing \"birb~authorize\".")
            return

        if message.content.startswith('birb~authorize'):
            await authorize_user(self, message)

        if message.content.startswith('birb~summary'):
            await summarize_settings(self, message)

        if message.content.startswith('birb~setflag'):
            await set_flag(self, message)

        if message.content.startswith('birb~addfight'):
            await add_fight(self, message)

        if message.content.startswith('birb~startfarm'):
            await start_farm(self, message)

        if message.content.startswith('birb~farminfo'):
            await farm_info(self, message)

        if message.content == 'birb~close':
            await self.close()


#DB Insertion and Querying methods
def check_db_exists():
    sql_create_fights = """ CREATE TABLE IF NOT EXISTS fight (
                        name TEXT PRIMARY KEY,
                        playeramount INTEGER NOT NULL
                        )  """

    sql_create_farms =  """ CREATE TABLE IF NOT EXISTS farm (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        begin_date TEXT NOT NULL,
                        fight_name INTEGER NOT NULL,
                        FOREIGN KEY (fight_name) REFERENCES fight (name)
                        )  """

    sql_create_players =    """ CREATE TABLE IF NOT EXISTS player (
                            name TEXT PRIMARY KEY,
                            activeMultiplier REAL NOT NULL
                            )  """

    sql_create_attendance = """ CREATE TABLE IF NOT EXISTS attendance (
                            farm_id INTEGER,
                            player_name TEXT,
                            is_helper INTEGER NOT NULL,
                            PRIMARY KEY(farm_id,player_name),
                            FOREIGN KEY (farm_id) REFERENCES farm (id),
                            FOREIGN KEY (player_name) REFERENCES player (name)
                            )  """

    sql_create_drops =  """ CREATE TABLE IF NOT EXISTS drops (
                        farm_id INTEGER,
                        player_name TEXT,
                        PRIMARY KEY(farm_id, player_name),
                        FOREIGN KEY (farm_id) REFERENCES farm (id),
                        FOREIGN KEY (player_name) REFERENCES player (name)
                        )  """

    sql_create_authorized =  """ CREATE TABLE IF NOT EXISTS authorized (
                            discord_name TEXT PRIMARY KEY,
                            admin INTEGER NOT NULL
                            )"""

    sql_create_settings =  """ CREATE TABLE IF NOT EXISTS flags (
                        flag TEXT PRIMARY KEY,
                        value REAL NOT NULL
                        )"""

    dbConnection.execute(sql_create_fights, sql_create_farms,
                        sql_create_players, sql_create_attendance,
                        sql_create_drops, sql_create_authorized,
                        sql_create_settings)

def load_bot_settings():
    sql_get_settings = """SELECT flag, value FROM flags"""
    flags = dict()
    for flag in dbConnection.query(sql_get_settings):
        flags[flag[0]] = flag[1]
    return flags

def load_authorized_users():
    sql_get_authorized = """SELECT discord_name FROM authorized"""
    users = []
    for user in dbConnection.query(sql_get_authorized):
        users.append(user[0])
    return users

def load_admins():
    sql_get_authorized = '''SELECT discord_name FROM authorized WHERE admin = 1'''
    users = []
    for user in dbConnection.query(sql_get_authorized):
        users.append(user[0])
    return users

def load_players():
    sql_get_users = '''SELECT name, activeMultiplier FROM player'''
    users = dict()
    for user in dbConnection.query(sql_get_users):
        users[user[0]] = user[1]
    return users

def load_fights():
    sql_get_fights = '''SELECT name, playeramount FROM fight'''
    fights = dict()
    for fight in dbConnection.query(sql_get_fights):
        fights[fight[0]] = fight[1]
    return fights

def load_farms():
    sql_get_farms = '''SELECT * FROM farm'''
    farms = dict()
    for farm in dbConnection.query(sql_get_farms):
        if(int(farm[0]) not in farms):
            farms[int(farm[0])] = dict()

        farms[int(farm[0])]["date"] = farm[1]
        farms[int(farm[0])]["fight"] = farm[2]
    return farms

def load_attendance():
    sql_get_attend = '''SELECT * FROM attendance'''
    attends = dict()
    for attend in dbConnection.query(sql_get_attend):
        if(int(attend[0]) not in attends):
            attends[int(attend[0])] = dict()

        #access: attend[farmId][playerName] = is_helper
        attends[int(attend[0])][attend[1]] = int(attend[2])
    return attends

def add_authorized_user(id, admin = 0):
    sql_add_authorized = '''INSERT INTO authorized (discord_name, admin) VALUES (?,?)'''
    return dbConnection.executeSafe(sql_add_authorized, id, admin)

def add_flag(key, value):
    sql_add_flag = '''INSERT INTO flags (flag, value) VALUES (?,?)'''
    return dbConnection.executeSafe(sql_add_flag, key, value)

def update_flag(key, value):
    sql_update_flag = '''UPDATE flags SET value = ? WHERE flag = ?'''
    return dbConnection.executeSafe(sql_update_flag, value, key)

def add_fight_db(name, numPlayers):
    sql_add_authorized = '''INSERT INTO fight (name, playeramount) VALUES (?,?)'''
    return dbConnection.executeSafe(sql_add_authorized, name, numPlayers)

def add_player_db(uid, multiplier):
    sql_add_player = '''INSERT INTO player (name, activeMultiplier) VALUES (?,?)'''
    return dbConnection.executeSafe(sql_add_player, str(uid), multiplier)

def add_farm_db(fightname):
    sql_add_farm = '''INSERT INTO farm (begin_date, fight_name) VALUES (DATETIME('now'), ?)'''
    return dbConnection.executeWithReturn(sql_add_farm, fightname)

def add_player_to_farm_db(farmid, name, helper=0):
    sql_add_to_farm = '''INSERT INTO attendance (farm_id, player_name, is_helper) VALUES (?,?,?)'''
    return dbConnection.executeWithReturn(sql_add_to_farm, farmid, name, helper)

#Bot response methods
async def authorize_user(client : MyClient, message : discord.Message):
    # check if someone is in the authorize list
    global authorized_users

    if not authorized_users:
        if(message.content == "birb~authorize"):
            if(add_authorized_user(str(message.author.id), 1)): 
                authorized_users.append(str(message.author.id))
                admins.append(str(message.author.id))
                await message.channel.send("Okay, " + message.author.mention + ", I've added you as the initial authorized user.")
                print("Added authorized user: ", message.author.id)
            else: 
                await message.channel.send("There was an error adding you as authorized user, " + message.author.mention + ". Please try again later.")
    #if someone is already authorized, only authorized users can add
    else: 
        if(str(message.author.id) not in authorized_users):
            await message.channel.send("Only an authorized user may grant permissions to others.")
        else:
            args = message.content.split(" ")
            if(len(args) != 2):
                await message.channel.send("Syntax: \"birb~authorize @user\".")
            else:
                #try to extract an id from the mention and find the user associated with it
                try:
                    user = user_by_mention(args[1])
                    uid = args[1][3:-1]
                    user = user_by_mention(args[1])
                    if user and uid not in authorized_users:
                        if(add_authorized_user(uid)): 
                            authorized_users.append(uid)
                            await message.channel.send("Okay, I've added " + user.mention + " as an authorized user.")
                            print("Added authorized user: ", uid)
                        else: 
                            await message.channel.send("There was an error adding" + user.mention + " as an authorized user. Please try again later.")
                    else: 
                        await message.channel.send("Could not find the mentioned user. Please try again later.")
                except Exception as e:
                    await message.channel.send("There was an error trying to find the mentioned user.")
                    print(e)

async def set_flag(client : MyClient, message : discord.Message):
    global set_flags
    success = False

    if not str(message.author.id) in authorized_users:
        await message.channel.send("Only authorized users may edit flags.")
    else:
        args = message.content.split(" ")
        if(len(args) != 3):
            await message.channel.send("Syntax: \"birb~setflag [key] [value]\".")
        else: 
            if(args[1] in set_flags):
                success = update_flag(args[1], args[2])
            else: 
                success = add_flag(args[1], args[2])

            if success:
                set_flags[args[1]] = args[2]
                await message.channel.send("Successfully set flag " + args[1] + " to value " + args[2] + ".")
                print("Set flag", args[1], "to", args[2])
            else:
                await message.channel.send("An internal error occurred trying to set the flag.")         

async def summarize_settings(client : MyClient, message : discord.Message):
    #Build embed
    embed = discord.Embed(title="Birb Bot Settings", color=0x00ff00)

    authorizedstr = ""
    for user in authorized_users:
        uinfo = client.get_user(int(user))
        if uinfo:
            authorizedstr += uinfo.name + "\n"

    fieldstr = ""
    for kvp in set_flags:
        fieldstr += kvp + ": " + str(set_flags[kvp]) + "\n"

    embed.add_field(name="Authorized Users", value=authorizedstr, inline=False)
    embed.add_field(name="Set Flags", value=fieldstr, inline=False)
    await message.channel.send(embed=embed)
    return

async def add_fight(client : MyClient, message : discord.Message):
    global fights
    success = False

    if not str(message.author.id) in authorized_users:
        await message.channel.send("Only authorized users may add new fights.")
    else:
        args = message.content.split(" ")
        if(len(args) != 3):
            await message.channel.send("Syntax: \"birb~addfight [name] [number of players per group]\".")
        elif (args[1] in fights):
            await message.channel.send("A fight with this name already exists.")
        else: 
            success = add_fight_db(args[1], args[2])

            if success:
                fights[args[1]] = int(args[2])
                await message.channel.send("Successfully added fight " + args[1] + " with player amount " + args[2] + ".")
                print("Added fight", args[1], "for", args[2], "players.")
            else:
                await message.channel.send("An internal error occurred trying to add the fight.")            

async def start_farm(client : MyClient, message : discord.Message):
    global attendance
    global farms

    if not str(message.author.id) in authorized_users:
        await message.channel.send("Only authorized users may start a farm run.")
    else:
        args = message.content.split(" ")
        if(len(args) < 3):
            await message.channel.send("Syntax: \"birb~startfarm [fight-name] [list of @mentions for participating players]\".")
        elif args[1] not in fights:
            await message.channel.send("The fight " + args[1] + " was not found. Please create it first using birb~addfight.")
        else: 
            if(len(args) != 2 + fights[args[1]]):
                
                def check(msg : discord.Message):
                    return msg.author == message.author and msg.content.startswith("Yes")

                await message.channel.send(message.author.mention + ", are you sure you want to start this farm with " + str(len(args) - 2) + 
                " players? It was saved in the database as intended for " + str(fights[args[1]]) + " players. Respond with 'Yes' if you want to continue.")
                
                try: 
                    await client.wait_for('message', check=check, timeout=20.0)
                except asyncio.TimeoutError:
                    await message.channel.send("Interrupted farm creation due to lack of player confirmation.")
                    return

            #check if all arguments are valid mentions
            for i in range(2, len(args)):
                try:
                    user = user_by_mention(args[i])
                    if not user:
                        await message.channel.send("Incorrect mention at argument: " + args[i])
                        return
                    else:
                        #check if user in DB, otherwise add with multiplier 1.0
                        if(str(user.id) not in players):
                            add_player(user.id)
                except:
                    await message.channel.send("Incorrect mention at argument: " + args[i])
                    return

            #players confirmed
            farmid = add_farm_db(fights[args[1]])
            if(not farmid):
                await message.channel.send("Error including farm into database. Please try again later.")
                return
                
            #farm created
            add_farm(farmid, str(date.today()), fights[args[1]])
            await message.channel.send("Created new farm for fight " + args[1] + ". Refer to this farm using ID " + str(farmid) + ".")

            for i in range (2, len(args)):
                pid = add_player_to_farm_db(farmid, args[i][3:-1])
                if(not pid):
                    await message.channel.send("Error adding player " +user_by_mention(args[i]).name + ". Please manually check which players are added to the farm and reassign the rest.")
                    return
                else:
                    add_attendance(farmid, pid, 0)
                    
                    
            
            #players added
            await message.channel.send("Successfully added " + str(len(args) - 2) + " players to the farm.")


async def farm_info(client : MyClient, message : discord.Message):
    args = message.content.split(" ")
    if(len(args) != 2):
        await message.channel.send("Syntax: \"birb~farminfo [farm_id]\".")
        return
    elif int(args[1]) not in farms:
        await message.channel.send("Could not find a farm with that farm ID.")
        return

    #Build embed
    embed = discord.Embed(title="Farm " + args[1], description="Fight: " + key_by_value(fights, farms[int(args[1])]["fight"]) ,color=0x00ff00)

    participantstr = ""
    for user in attendance[int(args[1])]:
        uinfo = client.get_user(int(user))
        if uinfo:
            participantstr += uinfo.name
            if(attendance[int(args[1])][user] != 0):
                participantstr += " (Helping)\n"
            else:
                participantstr += " (Farming)\n"

    embed.add_field(name="Participating Users", value=participantstr, inline=False)
    await message.channel.send(embed=embed)
    return

#helper methods
def user_by_mention(mention):
    uid = mention[3:-1]
    return client.get_user(int(uid))

def add_player(uid, multiplier = 1.0):
    global players
    if(add_player_db(uid, multiplier)):
        players[str(uid)] = multiplier
        print("Added user", uid, "with multiplier", multiplier)
        return True
    else:
        return False

def add_farm(id, date, fight):
    global farms

    if(int(id) not in farms):
        farms[int(id)] = dict()

        farms[int(id)]["date"] = date
        farms[int(id)]["fight"] = fight

def add_attendance(farmid, playerid, helper):
    global attendance
    if(int(farmid) not in attendance):
        attendance[int(farmid)] = dict()

    attendance[int(farmid)][playerid] = int(helper)

def key_by_value(dictionary, value):
    return list(dictionary.keys())[list(dictionary.values()).index(value)]

client = MyClient()
client.run(BOT_TOKEN)
